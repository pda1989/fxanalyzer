﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FXAnalyzer
{
    public class Analyzer
    {
        private IRatesRESTProvider _ratesProvider;
        private ILoger _loger;

        public Analyzer(IRatesRESTProvider ratesProvider, ILoger loger)
        {
            _ratesProvider = ratesProvider;
            _loger = loger;
        }

        public virtual IDictionary<string, decimal> CalculateTotalVolume(ITransactionsProvider transactionsProvider)
        {
            try
            {
                // Load transactions
                _loger?.Write("Loading transactions...");
                var transactions = transactionsProvider?.GetTransactions();
                _loger?.Write($"  Transactions count: {transactions?.Count ?? 0}");
                if (transactions == null)
                    return new Dictionary<string, decimal>();

                // Get rates
                _loger?.Write("Getting rates...");
                if (_ratesProvider != null)
                {
                    _ratesProvider.SendAllRequests(transactions);
                    transactions.ForEach(transaction =>
                        transaction.Rate = _ratesProvider.GetRate(transaction.BaseCurrency, transaction.CounterCurrency, transaction.ValueDate)); 
                }

                // Calculate total volume
                _loger?.Write("Calculating total volume...");
                var query = from transaction in transactions
                            group transaction.Amount * transaction.Rate by transaction.CounterCurrency into groupedTransactions
                            select new { Currency = groupedTransactions.Key, Volume = groupedTransactions.Sum() };
                return query.ToDictionary(item => item.Currency, item => item.Volume);
            }
            catch (Exception exception)
            {
                _loger?.Write($"Error: {exception.Message}");
                if (exception.InnerException != null)
                    _loger?.Write($"Error: {exception.InnerException.Message}");
                return new Dictionary<string, decimal>();
            }
        }
    }
}
