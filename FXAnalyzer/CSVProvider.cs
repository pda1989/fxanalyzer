﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace FXAnalyzer
{
    public class CSVProvider : ITransactionsProvider
    {
        private string _fileName;


        public CSVProvider(string fileName)
        {
            _fileName = fileName;
        }

        public static List<string> Split(string fileName, int maxRowCount)
        {
            if (string.IsNullOrEmpty(fileName))
                throw new ArgumentNullException("File name is empty");
            if (maxRowCount <= 0)
                throw new ArgumentNullException("MaxRowCount must be greater than 0");

            var fileNames = new List<string>();

            using (var file = new StreamReader(fileName))
            {
                string line;
                string currentFileName = Path.GetTempFileName(); 
                int count = 0;
                StringBuilder text = new StringBuilder();
                while ((line = file.ReadLine()) != null)
                {
                    if (string.IsNullOrEmpty(line))
                        continue;

                    if (count == maxRowCount)
                    {
                        File.WriteAllText(currentFileName, text.ToString());
                        fileNames.Add(currentFileName);
                        currentFileName = Path.GetTempFileName();
                        text.Clear();
                        count = 0;
                    }

                    text.AppendLine(line);

                    count++;
                }
                if (text.Length != 0)
                {
                    File.WriteAllText(currentFileName, text.ToString());
                    fileNames.Add(currentFileName);
                }
            }

            return fileNames;
        }

        public virtual List<Transaction> GetTransactions()
        {
            if (string.IsNullOrEmpty(_fileName))
                throw new ArgumentNullException("File name is empty");

            List<Transaction> transactions;
            using (var file = new StreamReader(_fileName))
                transactions = GetTransactions(file);

            return transactions;
        }

        public virtual List<Transaction> GetTransactions(TextReader file)
        {
            if (file == null)
                throw new ArgumentNullException();

            var transactions = new List<Transaction>();

            // Read transactions from the file
            string line;
            while ((line = file.ReadLine()) != null)
            {
                if (string.IsNullOrEmpty(line))
                    continue;

                var transaction = ParseValues(line);

                transactions.Add(transaction);
            }

            return transactions;
        }

        private Transaction ParseValues(string line)
        {
            var values = line.Split(',');

            if (values.Length != 5)
                throw new ArgumentException($"Wrong count of fields for transaction: {values.Length}, needed 5\nLine: \"{line}\"");
            if (!DateTime.TryParse(values[0], out DateTime tradeDate))
                throw new ArgumentException($"Cannot parse the date \"{values[0]}\"");
            if (!decimal.TryParse(values[3], NumberStyles.Any, CultureInfo.InvariantCulture, out decimal amount))
                throw new ArgumentException($"Cannot parse the value \"{values[3]}\"");
            if (!DateTime.TryParse(values[4], out DateTime valueDate))
                throw new ArgumentException($"Cannot parse the date \"{values[4]}\"");

            return new Transaction
            {
                TradeDate = tradeDate,
                BaseCurrency = values[1].Trim(),
                CounterCurrency = values[2].Trim(),
                Amount = amount,
                ValueDate = valueDate
            };
        }
    }
}
