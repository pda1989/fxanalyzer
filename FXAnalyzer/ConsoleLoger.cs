﻿using System;

namespace FXAnalyzer
{
    public class ConsoleLoger : ILoger
    {
        public virtual void Write(string message)
        {
            Console.WriteLine(message);
        }
    }
}
