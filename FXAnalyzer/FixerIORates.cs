﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;

namespace FXAnalyzer
{
    public class FixerIORates : IRatesRESTProvider
    {
        public class FixerIOResponse
        {
            public string Base { get; set; }
            public string Date { get; set; }
            public Rates Rates { get; set; }
        }


        // The field enables to limit the count of parallel tasks. It is used for the 
        // SendAllRequest() function, within ParallelOption
        private const int MaxTaskCount = 100;

        private IStoringProvider _requests;
        private IHttpProvider _httpClient;


        public FixerIORates(IStoringProvider requests, IHttpProvider httpClient)
        {
            _requests = requests;
            _httpClient = httpClient;
        }

        // The function enables to send all posible REST requests in parallel and store obtained 
        // information to the _requests dictionary. Next calls of function GetRate() will be
        // so much faster than without using SendAllRequests().
        public virtual void SendAllRequests(List<Transaction> transactions)
        {
            if (transactions == null || _httpClient == null)
                return;

            var posibleRequests = transactions
                .Select(transaction => new { BaseCurrency = transaction.BaseCurrency, ValueDate = transaction.ValueDate })
                .Distinct();

            Parallel.ForEach(posibleRequests, new ParallelOptions { MaxDegreeOfParallelism = MaxTaskCount }, request =>
            {
                var requestUri = GetUri(request.ValueDate, request.BaseCurrency);

                var fixerIOResponse = SendRequest(requestUri);
                _requests?.AddValue(requestUri, fixerIOResponse.Result.Rates);
            });
        }

        public virtual decimal GetRate(string baseCurrency, string counterCurrency, DateTime date)
        {
            return GetRateAsync(baseCurrency, counterCurrency, date).Result;
        }

        // The function sends REST request to get rates on demand. That is, if current request has 
        // been already obtained before, we don't need to send it again and the function returns 
        // stored value from _requests dictionary. Otherwise we send request and store it.
        public virtual async Task<decimal> GetRateAsync(string baseCurrency, string counterCurrency, DateTime date)
        {
            if (_httpClient == null)
                throw new ArgumentNullException("HttpClient cannot be empty");

            var requestUri = GetUri(date, baseCurrency);

            var rates = _requests?.GetValue(requestUri);
            if (rates != null)
                return rates.GetValueByName(counterCurrency);
            else
            {
                var fixerIOResponse = await SendRequest(requestUri);
                _requests?.AddValue(requestUri, fixerIOResponse.Rates);
                return fixerIOResponse.Rates.GetValueByName(counterCurrency);
            }
        }

        public virtual string GetUri(DateTime date, string currency)
        {
            var stringDate = date.ToString("yyyy-MM-dd");
            return $"http://api.fixer.io/{stringDate}?base={currency}";
        }

        protected async Task<FixerIOResponse> SendRequest(string uri)
        {
            if (_httpClient != null)
            {
                var message = await _httpClient?.GetStringAsync(uri);
                var fixerIOResponse = JsonConvert.DeserializeObject<FixerIOResponse>(message);
                return fixerIOResponse;
            }
            else
                return null;
        }
    }
}
