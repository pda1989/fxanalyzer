﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace FXAnalyzer
{
    public class HttpClientProvider : IHttpProvider, IDisposable
    {
        private HttpClient _client = new HttpClient();

        public async Task<string> GetStringAsync(string uri)
        {
            return await _client.GetStringAsync(uri);
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
