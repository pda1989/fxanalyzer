﻿using System;
using System.Threading.Tasks;

namespace FXAnalyzer
{
    public interface IHttpProvider
    {
        Task<string> GetStringAsync(string uri);
    }
}
