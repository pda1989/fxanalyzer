﻿using System;
using System.Collections.Generic;

namespace FXAnalyzer
{
    public interface IRatesProvider
    {
        decimal GetRate(string baseCurrency, string counterCurrency, DateTime valueDate);
    }
}
