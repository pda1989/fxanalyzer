﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FXAnalyzer
{
    public interface IRatesRESTProvider : IRatesProvider
    {
        void SendAllRequests(List<Transaction> transactions);
    }
}
