﻿namespace FXAnalyzer
{
    public interface IStoringProvider
    {
        void AddValue(string key, Rates value);
        Rates GetValue(string key);
    }
}
