﻿using System.Collections.Generic;

namespace FXAnalyzer
{
    public interface ITransactionsProvider
    {
        List<Transaction> GetTransactions();
    }
}
