﻿using System.Runtime.Caching;

namespace FXAnalyzer
{
    public class MemoryCacheProvider : IStoringProvider
    {
        private static MemoryCacheProvider _instance;
        private static object _lock = new object();

        public static MemoryCacheProvider Instance
        {
            get
            {
                lock (_lock)
                {
                    if (_instance == null)
                        _instance = new MemoryCacheProvider();
                    return _instance;
                }
            }
        }


        private MemoryCache _cache = new MemoryCache("MemoryCacheProvider");


        private MemoryCacheProvider() {}

        public void AddValue(string key, Rates value)
        {
            _cache.Set(key, value, new CacheItemPolicy()); 
        }

        public Rates GetValue(string key)
        {
            return _cache[key] as Rates; 
        }
    }
}
