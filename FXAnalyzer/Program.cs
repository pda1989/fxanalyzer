﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace FXAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var loger = new ConsoleLoger();

            if (args.Length == 0)
                loger?.Write("Error: Empty file name.\n\n usage: FXAnalyzer [<file name>]\n");
            else
            {
                var fileName = args[0];

                var stopWatch = new Stopwatch();
                stopWatch.Start();
                loger.Write("Start");

                try
                {
                    var results = new ConcurrentBag<IDictionary<string, decimal>>();
                    var fileNames = CSVProvider.Split(fileName, 1000);
                    using (var httpClient = new HttpClientProvider())
                    {
                        var analazer = new Analyzer(new FixerIORates(MemoryCacheProvider.Instance, httpClient), loger);
                        Parallel.ForEach(fileNames, splitedfileName =>
                        {
                            var currentResult = analazer.CalculateTotalVolume(new CSVProvider(splitedfileName));
                            results.Add(currentResult); 
                        });
                    }

                    var totalResult = results.SelectMany(result => result)
                                                      .GroupBy(item => item.Key, item => item.Value);
                    foreach (var item in totalResult)
                        loger?.Write($"  {item.Key} {item.Sum():F2}");
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }

                stopWatch.Stop();
                loger.Write($"Elapsed: {stopWatch.ElapsedMilliseconds} ms");
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey(); 
        }
    }
}
