﻿using System;

namespace FXAnalyzer
{
    public class Rates
    {
        public decimal EUR { get; set; }
        public decimal AUD { get; set; }
        public decimal BGN { get; set; }
        public decimal BRL { get; set; }
        public decimal CAD { get; set; }
        public decimal CHF { get; set; }
        public decimal CNY { get; set; }
        public decimal CZK { get; set; }
        public decimal DKK { get; set; }
        public decimal GBP { get; set; }
        public decimal HKD { get; set; }
        public decimal HRK { get; set; }
        public decimal HUF { get; set; }
        public decimal IDR { get; set; }
        public decimal ILS { get; set; }
        public decimal INR { get; set; }
        public decimal JPY { get; set; }
        public decimal KRW { get; set; }
        public decimal MXN { get; set; }
        public decimal MYR { get; set; }
        public decimal NOK { get; set; }
        public decimal NZD { get; set; }
        public decimal PHP { get; set; }
        public decimal PLN { get; set; }
        public decimal RON { get; set; }
        public decimal RUB { get; set; }
        public decimal SEK { get; set; }
        public decimal SGD { get; set; }
        public decimal THB { get; set; }
        public decimal TRY { get; set; }
        public decimal USD { get; set; }
        public decimal ZAR { get; set; }


        public virtual decimal GetValueByName(string currency)
        {
            switch (currency)
            {
                case "EUR": return EUR;
                case "AUD": return AUD;
                case "BGN": return BGN;
                case "BRL": return BRL;
                case "CAD": return CAD;
                case "CHF": return CHF;
                case "CNY": return CNY;
                case "CZK": return CZK;
                case "DKK": return DKK;
                case "GBP": return GBP;
                case "HKD": return HKD;
                case "HRK": return HRK;
                case "HUF": return HUF;
                case "IDR": return IDR;
                case "ILS": return ILS;
                case "INR": return INR;
                case "JPY": return JPY;
                case "KRW": return KRW;
                case "MXN": return MXN;
                case "MYR": return MYR;
                case "NOK": return NOK;
                case "NZD": return NZD;
                case "PHP": return PHP;
                case "PLN": return PLN;
                case "RON": return RON;
                case "RUB": return RUB;
                case "SEK": return SEK;
                case "SGD": return SGD;
                case "THB": return THB;
                case "TRY": return TRY;
                case "USD": return USD;
                case "ZAR": return ZAR;
                default: throw new ArgumentException();
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var rates = obj as Rates;

            return
                EUR == rates.EUR &&
                AUD == rates.AUD &&
                BGN == rates.BGN &&
                BRL == rates.BRL &&
                CAD == rates.CAD &&
                CHF == rates.CHF &&
                CNY == rates.CNY &&
                CZK == rates.CZK &&
                DKK == rates.DKK &&
                GBP == rates.GBP &&
                HKD == rates.HKD &&
                HRK == rates.HRK &&
                HUF == rates.HUF &&
                IDR == rates.IDR &&
                ILS == rates.ILS &&
                INR == rates.INR &&
                JPY == rates.JPY &&
                KRW == rates.KRW &&
                MXN == rates.MXN &&
                MYR == rates.MYR &&
                NOK == rates.NOK &&
                NZD == rates.NZD &&
                PHP == rates.PHP &&
                PLN == rates.PLN &&
                RON == rates.RON &&
                RUB == rates.RUB &&
                SEK == rates.SEK &&
                SGD == rates.SGD &&
                THB == rates.THB &&
                TRY == rates.TRY &&
                USD == rates.USD &&
                ZAR == rates.ZAR;
        }
    }
}