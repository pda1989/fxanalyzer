﻿using System;

namespace FXAnalyzer
{
    public class Transaction
    {
        public DateTime TradeDate { get; set; }
        public string BaseCurrency { get; set; }
        public string CounterCurrency { get; set; }
        public decimal Amount { get; set; }
        public decimal Rate { get; set; }
        public DateTime ValueDate { get; set; }

        public override string ToString() =>
            $"{TradeDate.ToString("yyyy-MM-dd")}: {BaseCurrency}/{CounterCurrency} - Amount: {Amount} Rate: {Rate}";
    }
}
