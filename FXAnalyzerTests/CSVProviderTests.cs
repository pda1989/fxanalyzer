﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;

namespace FXAnalyzer.Tests
{
    [TestClass()]
    public class CSVProviderTests
    {
        [TestMethod()]
        public void GetTransactions_String_GetsTransactions()
        {
            // Arrange
            var csv = new CSVProvider("Test");
            var tradeDate = new DateTime(2014, 03, 03);
            var valueDate = new DateTime(2014, 06, 03);

            // Act
            var transactions = csv.GetTransactions(new StringReader("2014-03-03,EUR,JPY,25000.00,2014-06-03"));

            // Assert
            Assert.AreEqual(1, transactions.Count);
            Assert.AreEqual(25000, transactions[0].Amount);
            Assert.AreEqual("EUR", transactions[0].BaseCurrency);
            Assert.AreEqual("JPY", transactions[0].CounterCurrency);
            Assert.AreEqual(tradeDate, transactions[0].TradeDate);
            Assert.AreEqual(valueDate, transactions[0].ValueDate);
        }
    }
}