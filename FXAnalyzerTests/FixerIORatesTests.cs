﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FXAnalyzer.Tests
{
    [TestClass()]
    public class FixerIORatesTests
    {
        [TestMethod()]
        public void SendAllRequests_Transaction_RatesStoredToCache()
        {
            // Arrange
            string response = "{\"Base\":\"EUR\", \"Date\":\"2000-01-01\",\"Rates\":{\"USD\":1}}";
            var date = new DateTime(2000, 1, 1);
            var mockHttpClient = new Mock<IHttpProvider>();
            mockHttpClient.Setup(h => h.GetStringAsync(It.IsAny<string>())).Returns(Task.Run(() => response));

            // Act
            var fixerIO = new FixerIORates(MemoryCacheProvider.Instance, mockHttpClient.Object);
            fixerIO.SendAllRequests(new List<Transaction>
            {
                new Transaction { Amount = 1, BaseCurrency = "EUR", CounterCurrency = "USD", TradeDate = date, ValueDate = date }
            });
            var rates = MemoryCacheProvider.Instance.GetValue(fixerIO.GetUri(date, "EUR"));

            // Assert
            Assert.IsNotNull(rates);
        }

        [TestMethod()]
        public void GetRate_EURUSD_GetsRateFromStoringProvider()
        {
            // Arrange
            var date = new DateTime(2000, 1, 1);
            int expectedRate = 1;
            var mockIStoringProvider = new Mock<IStoringProvider>();
            mockIStoringProvider.Setup(rp => rp.GetValue(It.IsAny<string>())).Returns(new Rates { USD = expectedRate });

            var fixerIO = new FixerIORates(mockIStoringProvider.Object, new HttpClientProvider());

            // Act
            var actualRate = fixerIO.GetRate("EUR", "USD", date);

            // Assert
            Assert.AreEqual(expectedRate, actualRate);
        }

        [TestMethod()]
        public void GetRate_EURUSD_GetsRateFromWeb()
        {
            // Arrange
            string response = "{\"Base\":\"EUR\", \"Date\":\"2000-01-01\",\"Rates\":{\"USD\":1}}";
            var date = new DateTime(2000, 1, 1);
            int expectedRate = 1;
            var mockHttpClient = new Mock<IHttpProvider>();
            mockHttpClient.Setup(h => h.GetStringAsync(It.IsAny<string>())).Returns(Task.Run(() => response));

            var fixerIO = new FixerIORates(MemoryCacheProvider.Instance, mockHttpClient.Object);

            // Act
            var actualRate = fixerIO.GetRate("EUR", "USD", date);

            // Assert
            Assert.AreEqual(expectedRate, actualRate);
        }

        [TestMethod()]
        public void GetUri_EUR_GetsUri()
        {
            // Arrange 
            var date = new DateTime(2000, 1, 1);
            var expectedUri = "http://api.fixer.io/2000-01-01?base=EUR";
            var fixerIO = new FixerIORates(MemoryCacheProvider.Instance, new HttpClientProvider());

            // Act
            var actualUri = fixerIO.GetUri(date, "EUR");

            // Assert
            Assert.AreEqual(expectedUri, actualUri);
        }
    }
}