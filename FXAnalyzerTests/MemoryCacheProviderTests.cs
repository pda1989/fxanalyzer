﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FXAnalyzer.Tests
{
    [TestClass()]
    public class MemoryCacheProviderTests
    {
        [TestMethod()]
        public void AddValue_Rates_GetsRates()
        {
            // Arrange
            var rates = new Rates();

            // Act
            MemoryCacheProvider.Instance.AddValue("key", rates);

            // Assert
            var actualRates = MemoryCacheProvider.Instance.GetValue("key");
            Assert.AreEqual(rates, actualRates);
        }

        [TestMethod()]
        public void GetValue_String_GetsRates()
        {
            // Arrange
            var rates = new Rates();
            MemoryCacheProvider.Instance.AddValue("key", rates);

            // Act
            var actualRates = MemoryCacheProvider.Instance.GetValue("key");

            // Assert
            Assert.AreEqual(rates, actualRates);
        }
    }
}