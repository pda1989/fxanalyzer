﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FXAnalyzer.Tests
{
    [TestClass()]
    public class RatesTests
    {
        [TestMethod()]
        public void GetValueByName_EUR_GetsRate()
        {
            // Arrange
            int expectedEUR = 1;
            var rates = new Rates { EUR = expectedEUR };

            // Act
            var actualEUR = rates.GetValueByName("EUR");

            // Assert
            Assert.AreEqual(expectedEUR, actualEUR);
        }
    }
}